# QuadTreeDemo

![](QuadTreeDemo.gif)

## Overview 
This is a Java/Processing 3 based demo to show how a quadtree stores and organizes objects. 
Quadtrees are useful for when you have a lot of objects in a scene at one time and need a 
better way to handle interactions between them. They are previlent in the gaming industry
as a large number of games require collision interactions and this limits the number of calculations
the simulation needs to run from O(n^n) to O(nlog(n)).

## Things I've learned from this project
* A quadtree is very similar to a linked list.
    * Its essentially a linked list but with 4 sub-links.
* A lot of recursion is used when using a quadtree because the quadtree has to know how to subdivide on its own.
* Just becuase a recursive function is used does not mean that it will lag the run time.

## How to Build
1.  Clone the project
2.  Build using Maven

## How to Use
* When you run the Application a window will open.
* White squares are to show the subdivisions of the quadtree. 
* The green square is just a bound that can quickly measure how many points are contained in it. 
    * That information is logged in the console.
* Clicking will add new points and will update the quadtree, subdividing as needed.

